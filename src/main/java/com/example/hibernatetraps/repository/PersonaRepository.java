
package com.example.hibernatetraps.repository;

import com.example.hibernatetraps.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Long> {

    void deleteByNombreContains(String fragmentoNombre);
}
