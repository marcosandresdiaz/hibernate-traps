package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;



@Service
@RequiredArgsConstructor
public class Trap01 {

    private final PersonaRepository personaRepository;
    
    /* 
    ** ¿Qué diferencia hay entre los dos métodos? 
    */
    @Transactional
    public void cambiarNombre1(String nombre) {
        Persona persona = personaRepository.findById(1L).get();
        persona.setNombre(nombre);
    }

    @Transactional
    public void cambiarNombre2(String nombre) {
        Persona persona = personaRepository.findById(1L).get();
        persona.setNombre(nombre);
        personaRepository.save(persona);
    }
}
