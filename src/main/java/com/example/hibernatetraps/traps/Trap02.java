package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap02 {

    private final PersonaRepository personaRepository;
    private final ClaseTransaccional claseTransaccional;
    
    /*
    * ¿Cuál es la diferencia entre "guardar1" y "guardar2"?
    */

    @Transactional
    public void guardar1() {
        Persona persona = new Persona("Nueva persona 1");
        personaRepository.save(persona);
        claseTransaccional.metodoQueFalla();
    }

    @Transactional
    public void guardar2() {
        try {
            claseTransaccional.metodoQueFalla();
        } catch (RuntimeException excepcion) {
            Persona persona = new Persona("Nueva persona 2");
            personaRepository.save(persona);
        }
    }
    
    
    @Service
    public static class ClaseTransaccional {

        @Transactional
        public void metodoQueFalla() {
            throw new RuntimeException("Error!");
        }
    }


}
