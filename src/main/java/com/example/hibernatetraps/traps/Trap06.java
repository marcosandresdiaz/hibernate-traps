package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap06 {

    private final PersonaRepository personaRepository;

    /** 
    * ¿Cuál es la diferencia entre hacer getOne y findById.get?
    * (Analizar el caso en el que existe el ID y en el que no existe)
    */
    public Persona buscarPorId1(Long id) {
        return personaRepository.findById(id).get();
    }

    public Persona buscarPorId2(Long id) {
        return personaRepository.getOne(id);
    }
}
