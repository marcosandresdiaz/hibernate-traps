package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Transactional
@RequiredArgsConstructor
public class Trap03 {
    
    private final PersonaRepository personaRepository;

    /**
    * ¿Qué queries habrá ejecutado Hibernate luego de renderizar la página en "/"?
    * El template de la página es este (en lenguaje Thymeleaf):
    *   <html>
    *       <body>
    *           <p th:text="'Hola!' + ${persona.nombre}"></p>
    *       </body>
    *   </html>
    */
    @GetMapping("/")
    public String verPersona1(Model model) {
        Persona persona = personaRepository.findById(1L).get();
        model.addAttribute("persona", persona);
        return "trap3";
    }
}
