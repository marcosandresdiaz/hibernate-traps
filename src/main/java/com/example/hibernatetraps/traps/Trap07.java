package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.repository.PersonaRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap07 {

    private final PersonaRepository personaRepository;

    /* 
    ** ¿Qué queries ejecuta esto? (Hay dos personas en la base de datos)
    */
    @Transactional
    public void borrarPersonasQueContienenPepe() {
        personaRepository.deleteAll();
    }

}
