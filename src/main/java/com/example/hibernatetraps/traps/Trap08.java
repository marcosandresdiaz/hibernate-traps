package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap08 {

    private final PersonaRepository personaRepository;

    /* 
    ** ¿Cómo queda la base de datos después de ejecutar "guardar"?
     */
    
    // Este método no es @Transactional
    public void guardar() {
        guardarInterno();
    }

    @Transactional
    private void guardarInterno() {
        personaRepository.save(new Persona("Pepe"));
        throw new RuntimeException("Error!");
    }

}
