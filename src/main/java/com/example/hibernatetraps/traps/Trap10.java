package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap10 {

    private final PersonaRepository personaRepository;

    /**
     * ¿Qué queries ejecuta este método?
     */
    @Transactional
    public void guardarPersonaConIdExistente() {
        Persona persona = new Persona("Nombre cambiado");
        persona.setId(1L);
        personaRepository.save(persona);
    }
    
    /**
     * ¿Y este?
     */
    @Transactional
    public void guardarPersonaYCambiarId() {
        Persona persona = new Persona("Nombre cambiado");
        persona.setId(1L);
        personaRepository.save(persona);
        persona.setId(50L);
        persona.setNombre("Nombre cambia2");
    }


}
