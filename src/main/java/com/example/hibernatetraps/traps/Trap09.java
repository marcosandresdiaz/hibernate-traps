package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import java.util.stream.Stream;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap09 {

    private final ClaseTransaccional claseTransaccional;

    /**
     * ¿Cómo queda la persona de id 1 después de ejecutar esto?
     *
     */
    public void actualizarPersona1() {
        correrEnParalelo(
                () -> claseTransaccional.cambiarNombre(),
                () -> claseTransaccional.cambiarApellido());
    }

    @Service
    @RequiredArgsConstructor
    public static class ClaseTransaccional {

        private final PersonaRepository personaRepository;

        @Transactional
        @SneakyThrows
        public void cambiarNombre() {
            System.out.println("< Empieza cambiarNombre");
            Persona persona = personaRepository.findById(1L).get();
            persona.setNombre("Nombre cambiado");
            Thread.sleep(1000);
            System.out.println("> Termina cambiarNombre");
        }

        @Transactional
        @SneakyThrows
        public void cambiarApellido() {
            Thread.sleep(500);
            System.out.println("  < Empieza cambiarApellido");
            Persona persona = personaRepository.findById(1L).get();
            persona.setApellido("Apellido cambiado");
            System.out.println("  > Termina cambiarApellido");
        }

    }

    private void correrEnParalelo(Runnable ... runnables) {
        Stream.of(runnables).parallel().forEach(Runnable::run);
    }

}
