package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap11 {

    private final PersonaRepository personaRepository;

    /**
     * ¿Cómo queda la tabla al final de este método?
     */
    @Transactional
    public void crearPersonaConFlush() {
        Persona persona = new Persona("Nombre cambiado");
        personaRepository.saveAndFlush(persona);
        throw new RuntimeException("Error!");
    }


}
