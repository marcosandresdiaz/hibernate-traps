package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap05 {

    private final EntityManager entityManager;

    /* 
    ** ¿Qué queries ejecuta este método y en qué líneas?
    */
    @Transactional
    public void crearPersona() {
        Persona persona = new Persona("Pepe");
        entityManager.persist(persona);
        entityManager.detach(persona);

        entityManager.flush();
    }

}
