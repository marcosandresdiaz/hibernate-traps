
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.domain.Persona;
import com.example.hibernatetraps.repository.PersonaRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Trap04 {
    
    private final PersonaRepository personaRepository;

    /*
    ** ¿Qué queries ejecuta este método y en qué líneas?
    */
    @Transactional
    public void crearPersona() {
        Persona personaNueva = new Persona("Pepito");
        personaRepository.save(personaNueva);
    }
}
