DROP TABLE IF EXISTS mascota;
DROP TABLE IF EXISTS persona;

CREATE TABLE persona (
    id INT IDENTITY,
    nombre VARCHAR(100) NOT NULL,
    apellido VARCHAR(100)
);

CREATE TABLE mascota (
    id INT IDENTITY,
    nombre VARCHAR(100) NOT NULL,
    persona_id INT REFERENCES persona(id)
);

DELETE FROM persona;
INSERT INTO persona (id, nombre, apellido) VALUES (1, 'Persona 1', 'Apellido 1');
INSERT INTO persona (id, nombre, apellido) VALUES (2, 'Persona 2', 'Apellido 2');