
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap08Tests extends ApplicationTests {

    @Autowired
    Trap08 trap8;
    
    @Test
    public void test() throws Exception {
        print("TRAP 8");
        List<String> personasAntes = queryJdbcNombresPersonas();
        
        try {
            trap8.guardar();
        } catch (RuntimeException ex) {
        }
        
        List<String> personasDespues = queryJdbcNombresPersonas();
        
        print("Personas antes:");
        print(personasAntes);
        print("Personas despues:");
        print(personasDespues);
    }
}
