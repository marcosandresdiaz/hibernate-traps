
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap11Tests extends ApplicationTests {

    @Autowired
    Trap11 trap11;
    
    @Test
    public void test_guardarConId() throws Exception {
        print("TRAP 11");
        List<String> personasAntes = queryJdbcNombresPersonas();
        
        try {
            trap11.crearPersonaConFlush();
        } catch (Exception ex) {
        }
        
        List<String> personasDespues = queryJdbcNombresPersonas();
        print("Personas antes:");
        print(personasAntes);
        print("Personas despues:");
        print(personasDespues);
    }
    
}
