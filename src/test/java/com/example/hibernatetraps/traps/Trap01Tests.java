
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap01Tests extends ApplicationTests {
    
    @Autowired
    Trap01 trap1;
    
    @Test
    public void test1() {
        print("TRAP 1: cambiarNombre1");
        String nombreAntes = jdbcTemplate.queryForObject("SELECT nombre FROM persona WHERE id = 1", String.class);
        
        trap1.cambiarNombre1("Nombre1");
        
        String nombreDespues = jdbcTemplate.queryForObject("SELECT nombre FROM persona WHERE id = 1", String.class);
        print("Nombre antes: " + nombreAntes);
        print("Nombre después: " + nombreDespues);
    }
    
    @Test
    public void test2() {
        print("TRAP 1: cambiarNombre2");
        String nombreAntes = jdbcTemplate.queryForObject("SELECT nombre FROM persona WHERE id = 1", String.class);
        
        trap1.cambiarNombre2("Nombre2");
        
        String nombreDespues = jdbcTemplate.queryForObject("SELECT nombre FROM persona WHERE id = 1", String.class);
        print("Nombre antes: " + nombreAntes);
        print("Nombre después: " + nombreDespues);
    }
}
