
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap10Tests extends ApplicationTests {

    @Autowired
    Trap10 trap10;
    
    @Test
    public void test_guardarConId() throws Exception {
        print("TRAP 10: guardar con id ya puesto");
        List<String> personasAntes = queryJdbcNombresPersonas();
        
        trap10.guardarPersonaConIdExistente();
        
        List<String> personasDespues = queryJdbcNombresPersonas();
        print("Personas antes:");
        print(personasAntes);
        print("Personas despues:");
        print(personasDespues);
    }
    
    @Test
    public void test_guardarConIdYLuegoCambiarId() throws Exception {
        print("TRAP 10: guardar con id ya puesto y cambiarlo");
        List<String> personasAntes = queryJdbcNombresPersonas();
        
        trap10.guardarPersonaYCambiarId();
        
        List<String> personasDespues = queryJdbcNombresPersonas();
        print("Personas antes:");
        print(personasAntes);
        print("Personas despues:");
        print(personasDespues);
    }
}
