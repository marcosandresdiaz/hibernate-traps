
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@AutoConfigureMockMvc(print = MockMvcPrint.NONE) // Si imprimo, el atrevido le hace el get a la colección...
public class Trap03Tests extends ApplicationTests {

    @Autowired
    MockMvc mockMvc;
    
    @Test
    public void test1() throws Exception {
        print("TRAP 3");
        mockMvc.perform(MockMvcRequestBuilders.get("/"));
    }
}
