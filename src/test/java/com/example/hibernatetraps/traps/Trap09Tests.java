
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap09Tests extends ApplicationTests {

    @Autowired
    Trap09 trap9;
    
    @Test
    public void test() throws Exception {
        print("TRAP 9");
        String nombreAntes = jdbcTemplate.queryForObject("SELECT nombre FROM persona WHERE id = 1", String.class);
        String apellidoAntes = jdbcTemplate.queryForObject("SELECT apellido FROM persona WHERE id = 1", String.class);
        
        trap9.actualizarPersona1();
        
        String nombreDespues = jdbcTemplate.queryForObject("SELECT nombre FROM persona WHERE id = 1", String.class);
        String apellidoDespues = jdbcTemplate.queryForObject("SELECT apellido FROM persona WHERE id = 1", String.class);
        
        print("Persona 1 antes:");
        print("Nombre: " + nombreAntes + ", Apellido: " + apellidoAntes);
        print("Persona 1 despues:");
        print("Nombre: " + nombreDespues + ", Apellido: " + apellidoDespues);
    }
}
