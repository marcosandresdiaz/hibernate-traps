
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap02Tests extends ApplicationTests {
    
    @Autowired
    Trap02 trap2;
    
    @Test
    public void test1() {
        print("TRAP 2: guardar1");
        List<String> personasAntes = queryJdbcNombresPersonas();
        
        try {
            trap2.guardar1();
        } catch (RuntimeException excepcion) {
        }
        
        List<String> personasDespues = queryJdbcNombresPersonas();
        print("Personas originales");
        print(personasAntes);
        print("Personas luego de guardar1");
        print(personasDespues);
        
    }
    
    @Test
    public void test2() {
        print("TRAP 2: guardar2");
        List<String> personasAntes = queryJdbcNombresPersonas();
        
        try {
            trap2.guardar2();
        } catch (RuntimeException excepcion) {
        }
        
        List<String> personasDespues = queryJdbcNombresPersonas();
        print("Personas originales");
        print(personasAntes);
        print("Personas luego de guardar2");
        print(personasDespues);
        
    }
}
