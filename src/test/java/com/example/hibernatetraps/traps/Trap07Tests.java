
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap07Tests extends ApplicationTests {

    @Autowired
    Trap07 trap7;
    
    @Test
    public void test() throws Exception {
        print("TRAP 7");
        trap7.borrarPersonasQueContienenPepe();
    }
}
