
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap06Tests extends ApplicationTests {

    @Autowired
    Trap06 trap6;
    
    @Test
    public void test_findById_existe() throws Exception {
        print("TRAP 6: findById( idExistente ).get().getNombre()");
        print(trap6.buscarPorId1(1L).getNombre());
    }
    
    @Test
    public void test_getOne_existe() throws Exception {
        print("TRAP 6: getOne( idExistente ).getNombre()");
        try {
            print(trap6.buscarPorId2(1L).getNombre());
        } catch (Exception ex) {
            print(ex);
        }
    }
    
    @Test
    public void test_findById_noExiste() throws Exception {
        print("TRAP 6: findById( idInexistente ).get().getNombre()");
        try {
            print(trap6.buscarPorId1(999L).getNombre());
        } catch (Exception ex) {
            print(ex);
        }
    }
    
    @Test
    public void test_getOne_noExiste() throws Exception {
        print("TRAP 6: getOne( idInexistente ).getNombre()");
        try {
            print(trap6.buscarPorId2(999L).getNombre());
        } catch (Exception ex) {
            print(ex);
        }
    }
}
