
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap05Tests extends ApplicationTests {

    @Autowired
    Trap05 trap5;
    
    @Test
    public void test() throws Exception {
        print("TRAP 5");
        trap5.crearPersona();
    }
}
