
package com.example.hibernatetraps.traps;

import com.example.hibernatetraps.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Trap04Tests extends ApplicationTests {

    @Autowired
    Trap04 trap4;
    
    @Test
    public void test() throws Exception {
        print("TRAP 4");
        trap4.crearPersona();
    }
}
