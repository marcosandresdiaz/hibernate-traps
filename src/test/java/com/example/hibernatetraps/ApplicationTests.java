package com.example.hibernatetraps;

import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(value = "/schema.sql")
public abstract class ApplicationTests {
    
    @Autowired
    protected JdbcTemplate jdbcTemplate;
    
    protected void print(Object object) {
        System.out.print("* ");
        System.out.println(object);
    }
    
    @AfterEach
    public void separador() {
        System.out.println();
        System.out.println();
    }
    
    protected List<String> queryJdbcNombresPersonas() {
        return jdbcTemplate.queryForList("SELECT nombre FROM persona", String.class);
    }
    
}
